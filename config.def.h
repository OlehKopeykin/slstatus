/* See LICENSE file for copyright and license details. */

/* interval between updates (in ms) */
const unsigned int interval = 100;

/* text to show if no value can be retrieved */
static const char unknown_str[] = "n/a";

/* maximum output string length */
#define MAXLEN 2048

/* maximum single item length */
#define MAXLEN_SITEM 100

static char item_text[MAXLEN_SITEM];

const char *
temp_ext(const char *file) 
{
	const char *res;
	uintmax_t tempr;
	// Use Nerd Fonts https://github.com/ryanoasis/nerd-fonts
	const char *icon_strings[11] = { "", "", "", "", "", "", "", "", "", "", "" };

	if (!(res = temp(file))) {
		return unknown_str;
	} else if (sscanf(res, "%ju", &tempr) != 1) {
		return unknown_str;
	}

	if((tempr / 105) > 0) {
		tempr = 105; // Max temp is 105°C.
	}
	tempr /= 10;  // Temp step is 10°C.

	if (esnprintf(item_text, MAXLEN_SITEM, "%s %s°C", icon_strings[tempr], res) < 0) {
		return unknown_str;
	}
	return item_text;
}

const char *
batt_ext(const char *batt_item) 
{
	const char *res;
	uintmax_t batt;
	char stat;
	const char *batt_lvl_icon;
	const char *batt_stat_icon = "";
	// Use Nerd Fonts https://github.com/ryanoasis/nerd-fonts
	const char *icon_strings[11] = { "", "", "", "", "", "", "", "", "", "", "" };

	if (!(res = battery_perc(batt_item))) {
		return unknown_str;
	} else if (sscanf(res, "%ju", &batt) != 1) {
		return unknown_str;
	}

	if (!(res = battery_state(batt_item))) {
		return unknown_str;
	} else if (sscanf(res, "%c", &stat) != 1) {
		return unknown_str;
	}

	if((batt / 100) > 0) {
		batt = 100; // Max battery level is 100%.
	}
	batt_lvl_icon = icon_strings[batt / 10]; // Battery level step is 10%.

	if (batt == 100 && stat == 'o') {
		// Use Nerd Fonts https://github.com/ryanoasis/nerd-fonts
		batt_lvl_icon = "";
		batt_stat_icon="";
	} else if (stat == '+') {
		// Use Nerd Fonts https://github.com/ryanoasis/nerd-fonts
		batt_stat_icon = "";
	}

	if (esnprintf(item_text, MAXLEN_SITEM,
	              "%s%s %ju%%", batt_stat_icon, batt_lvl_icon, batt) < 0) {
		return unknown_str;
	}
	return item_text;
}

/*
 * function            description                     argument (example)
 *
 * battery_perc        battery percentage              battery name (BAT0)
 *                                                     NULL on OpenBSD/FreeBSD
 * battery_remaining   battery remaining HH:MM         battery name (BAT0)
 *                                                     NULL on OpenBSD/FreeBSD
 * battery_state       battery charging state          battery name (BAT0)
 *                                                     NULL on OpenBSD/FreeBSD
 * cat                 read arbitrary file             path
 * cpu_freq            cpu frequency in MHz            NULL
 * cpu_perc            cpu usage in percent            NULL
 * datetime            date and time                   format string (%F %T)
 * disk_free           free disk space in GB           mountpoint path (/)
 * disk_perc           disk usage in percent           mountpoint path (/)
 * disk_total          total disk space in GB          mountpoint path (/)
 * disk_used           used disk space in GB           mountpoint path (/)
 * entropy             available entropy               NULL
 * gid                 GID of current user             NULL
 * hostname            hostname                        NULL
 * ipv4                IPv4 address                    interface name (eth0)
 * ipv6                IPv6 address                    interface name (eth0)
 * kernel_release      `uname -r`                      NULL
 * keyboard_indicators caps/num lock indicators        format string (c?n?)
 *                                                     see keyboard_indicators.c
 * keymap              layout (variant) of current     NULL
 *                     keymap
 * load_avg            load average                    NULL
 * netspeed_rx         receive network speed           interface name (wlan0)
 * netspeed_tx         transfer network speed          interface name (wlan0)
 * num_files           number of files in a directory  path
 *                                                     (/home/foo/Inbox/cur)
 * ram_free            free memory in GB               NULL
 * ram_perc            memory usage in percent         NULL
 * ram_total           total memory size in GB         NULL
 * ram_used            used memory in GB               NULL
 * run_command         custom shell command            command (echo foo)
 * swap_free           free swap in GB                 NULL
 * swap_perc           swap usage in percent           NULL
 * swap_total          total swap size in GB           NULL
 * swap_used           used swap in GB                 NULL
 * temp                temperature in degree celsius   sensor file
 *                                                     (/sys/class/thermal/...)
 *                                                     NULL on OpenBSD
 *                                                     thermal zone on FreeBSD
 *                                                     (tz0, tz1, etc.)
 * uid                 UID of current user             NULL
 * uptime              system uptime                   NULL
 * username            username of current user        NULL
 * vol_perc            OSS/ALSA volume in percent      mixer file (/dev/mixer)
 *                                                     NULL on OpenBSD/FreeBSD
 * wifi_essid          WiFi ESSID                      interface name (wlan0)
 * wifi_perc           WiFi signal in percent          interface name (wlan0)
 */

// Use Nerd Fonts https://github.com/ryanoasis/nerd-fonts
static const struct arg args[] = {
	/* function      format                argument */
	{ cpu_perc,      " ❮  %s%%",         NULL },
	{ temp_ext,      " ❮ %s",             "/sys/class/thermal/thermal_zone0/temp" },
	{ ram_free,      " ❮  %s",           NULL },
	{ disk_free,     " ❮  /h:%s",        "/home" },
	{ disk_free,     " | /:%s",           "/" },
	{ batt_ext,      " ❮ %s",             "BAT0" },
	{ keymap,        " ❮   %s",           NULL },
	{ datetime,      " ❮ %s",             "%F" },
	{ datetime,      " ❮ %s",             "%T" },
};
